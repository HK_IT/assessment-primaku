/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        gray700: '#2D3748',
        gray500: '#718096',
        gray200: '#E2E8F0'
      }
    },
    fontFamily: {
      'sans': ['Helvetica']
    }
  },
  plugins: [],
}
