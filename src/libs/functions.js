export const getImgUrl = (url) => {
    return new URL(url, import.meta.url).href;
}