import { createRouter, createWebHistory } from 'vue-router'
import Home from './components/pages/home/Index.vue'
import Tables from './components/pages/tables/Index.vue'
import Billing from './components/pages/billing/Index.vue'

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
    },
    {
        path: '/tables',
        name: 'Tables',
        component: Tables,
    },
    {
        path: '/billing',
        name: 'Billing',
        component: Billing,
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes,
})

export default router